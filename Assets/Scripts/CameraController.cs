﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using GamepadInput;

public class CameraController : MonoBehaviour {
    
    public GamePad.Index gamepadIndex;
    
    public List<Transform> targets = new List<Transform>();
    public float followSpeed = 4f;
    public float movementSpeed = 4f;
    public float zOffset = -8;

    private enum MovementType { Follow, Free };
    private MovementType movementType = MovementType.Follow;

    private Transform target;

    private Vector3 position { get { return transform.position; } }

    private void Awake() {
        SetTarget(targets[0]);
    }

    private void LateUpdate() {
        if (GamePad.GetButtonDown(GamePad.Button.RightStick, gamepadIndex)) {
            SetTarget(GetClosestsTarget());
        }

        if (GamePad.GetButtonDown(GamePad.Button.LeftShoulder, gamepadIndex)) {
            SetTarget(GetPreviousTarget());
        }

        if (GamePad.GetButtonDown(GamePad.Button.RightShoulder, gamepadIndex)) {
            SetTarget(GetNextTarget());
        }

        Vector2 axis = GamePad.GetAxis(GamePad.Axis.LeftStick, gamepadIndex);

        if(movementType == MovementType.Follow && axis.magnitude > 0.1f) {
            movementType = MovementType.Free;
        }

        if(movementType == MovementType.Free) {
            transform.position += new Vector3(axis.x, 0, axis.y) * movementSpeed * Time.deltaTime;
        }else if(movementType == MovementType.Follow) {
            transform.position = Vector3.Lerp(position, new Vector3(target.position.x, position.y, target.position.z + zOffset), followSpeed * Time.deltaTime);
        }
    }

    private void SetTarget(Transform target) {
        movementType = MovementType.Follow;
        this.target = target;
    }

    private Transform GetPreviousTarget() {
        int index = targets.IndexOf(target);
        return targets[(index - 1 >= 0 ? index - 1 : targets.Count - 1)];
    }

    private Transform GetNextTarget() {
        int index = targets.IndexOf(target);
        return targets[(index + 1 < targets.Count ? index + 1 : 0)];
    }

    private Transform GetClosestsTarget() {
        return targets.OrderBy(t => Vector2.Distance(new Vector2(t.position.x, t.position.z), new Vector2(position.x, position.z - zOffset))).ToList()[0];
    }
}